using NUnit.Framework;
using PSP0_1_CalculoDeAreas_dotNetCore;
using System;

namespace CalculoDeAreas.UnitTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAreaCirculo_RegresaDecimal()
        {
            //arrange
            var area = new Areas();
            //var PI = Math.PI;
            //act
            //recibe como radio un decimal
            decimal radio = 34.23425234656235m;
            decimal res = area.circulo(radio);
            //assertt
     
            Assert.AreEqual(3681.896430484762058222445051m, res);
        }
        [Test]
        public void TestAreaRectangulo_RegresaDecimal()
        {
            //arrange
            Areas area = new Areas();
            //act
            decimal res = area.rectangulo(333.333m, 44.44m);
            decimal esperado = 333.333m * 44.44m ;
            //assert
            Assert.AreEqual(esperado, res);

        }
        [Test]
        public void TestAreaTriangulo_RegresaDecimal()
        {
            //arrange
            Areas areas = new Areas();
            //act
            decimal res= areas.triangulo(333.333m, 44.44m);
            //assert
            Assert.AreEqual(333.333m * 44.44m / 2, res);

        }
        [Test]
        public void TestAreaCuadrado_RegresaDecimal()
        {
            //arrange
            Areas areas = new Areas();
            //act
            decimal res= areas.cuadrado(333.333m);
            //assert
            Assert.AreEqual(333.333m * 333.333m, res);

        }
    }
}