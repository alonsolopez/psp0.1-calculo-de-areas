﻿using System;
using PSP0_1_CalculoDeAreas_dotNetCore;
namespace PSP0_1_CalculoDeAreas_Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Prueba de Libreria de Clases - Calculo de Áreas");
            Console.WriteLine("------------------------------------------------");
            decimal r = 222;
            decimal B = 223;
            decimal h = 224;
            decimal L = 225;
            Console.WriteLine("Con estos Datos de Entrada... ");
            Console.WriteLine("radio="+r);
            Console.WriteLine("Base="+B);
            Console.WriteLine("Altura="+h);
            Console.WriteLine("Lado="+L);
            Console.WriteLine("------------------------------------------------");
            Areas areas = new Areas();
            Console.WriteLine("Àrea del Cìrculo con radio="+r+", ="+areas.circulo(r));
            Console.WriteLine("Àrea del Rectángulo con B="+B+" y h="+h+", ="+areas.rectangulo(B,h));
            Console.WriteLine("Àrea del Triángulo con B="+B+" y h="+h+", ="+areas.triangulo(B,h));
            Console.WriteLine("Àrea del Cuadrado con Lado="+L+", ="+areas.cuadrado(L));
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Presiona cualquier tecla para continuar");
            //Console.ReadKey();
        }
    }
}
