﻿//psedo

paquete: AreasDeFiguras
class: Areas


métodos:
circulo(decimal r)

INICIO
    recibe r;
    regresa Math.PI*Math.pow(r,2); 
FIN

rectangiulo(decimal B, decimal h)

INICIO
    recibe B, h;
    regresa B*h; 
FIN

triangulo(decimal B, decimal h)

INICIO
    recibe B, h;
    regresa B*h/2; 
FIN

cuadrado(decimal L)

INICIO
    recibe L;
    regresa L*L; 
FIN
