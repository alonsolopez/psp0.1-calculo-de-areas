﻿
#Requisitos para proyecto Calculo de Áreas TDD
---
##Descripcion:
###Una biblioteca de clases, MULTIPLATAFORMA, que pueda calcularnos el área de figuras geopmetricas como CIRCULO, RECTANGULO, CUADRADO, TRIANGULO
---

##Reqs FUNCIONALES
###**RF01:** El sistema debe permitir la obtencion del area del circulo tomando como dato de entrada el RADIO, de tipo decimal. El resultado también se obtiene de tipo decimal.
```
A = PI * r^2
```

###**RF02:** El sistema debe permitir la obtencion del area del rectángulo tomando como datos de entrada la BASE(B) y ALTURA(h), de tipo decimal ambos. El resultado también se obtiene de tipo decimal.
```
A = B * h
```

###**RF03:** El sistema debe permitir la obtencion del area del triángulo tomando como datos de entrada la BASE(B) y ALTURA(h), de tipo decimal ambos. El resultado también se obtiene de tipo decimal.
```
A = B * h / 2
```

###**RF04:** El sistema debe permitir la obtencion del area del cuadrado tomando como datos de entrada el LADO, de tipo decimal . El resultado también se obtiene de tipo decimal.
```
A = L^2
```

---

##REQS NO FUNCIONALES
**Docu...**
###**RNF01:** Los métodos deben tener bloques de comentarios bien específicos, y claros para su implementación.


**Consideración para la estimación de LOCs**
--1 loc def
--1 loc pars extra!!
```
def metodo(par, [pas...])
{
    //...instrucción RETURN 1 LOC
}
``
