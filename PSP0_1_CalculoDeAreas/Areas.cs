﻿using System;
namespace PSP0_1_CalculoDeAreas
{
    public class Areas
    {
        public Areas()
        {
        }

        public decimal circulo(decimal r)
        {
            //return Math.PI * r*r;
            return 2m * decimal.Parse(Math.Acos(0.0).ToString()) * r*r;
        }

        public decimal rectangulo(decimal B, decimal h)
        {
            return B*h;
        }

        public decimal triangulo(decimal B, decimal h)
        {
            return B*h/2;
        }

        public decimal cuadrado(decimal v)
        {
            return v * v;
        }
    }
}
