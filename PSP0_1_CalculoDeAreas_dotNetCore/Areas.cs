﻿using System;
namespace PSP0_1_CalculoDeAreas_dotNetCore
{
    /// <summary>
    /// Clase para el cálculo de áreas de figuras geométricas básicas
    /// CUADRADO, TRIANGULO, CIRCULO, RECTANGULO.
    /// Cáda método define sus parametros de ENTRADA.
    /// todos los datos son de tipo decimal.
    /// Usage: Areas obj = new Areas(); obj.circulo('tu radio, de tipo decimal');
    /// </summary>
    public class Areas
    {
        public Areas()
        {
        }

        /// <summary>
        /// Calcula el área del círculo, mediante el radio introducido en los parámetros.
        /// El resultado es de tipo decimal, así como el radio de entrada.
        /// </summary>
        /// <param name="r">El rádio para el cálculo del área</param>
        /// <returns>El área del circulo con 'r' en los parámetros</returns>
        public decimal circulo(decimal r)
        {
            //return Math.PI * r*r;
            return 2m * decimal.Parse(Math.Acos(0.0).ToString()) * r*r;
        }
        /// <summary>
        /// Calcula el área del rectángulo, mediante la B(base) y h(altura) introducidos en los parámetros.
        /// El resultado es de tipo decimal, así como el B y h de entrada.
        /// </summary>
        /// <param name="B">la Base del rectángulo a calcular su área</param>
        /// <param name="h">la altura del rectángulo a calcular su área</param>
        /// <returns>El área del rectángulo con 'B' y 'h' en los parámetros</returns>
        public decimal rectangulo(decimal B, decimal h)
        {
            return B*h;
        }
        /// <summary>
        /// Calcula el área del triángulo, mediante la B(base) y h(altura) introducidos en los parámetros.
        /// El resultado es de tipo decimal, así como el B y h de entrada.
        /// </summary>
        /// <param name="B">la Base del triángulo a calcular su área</param>
        /// <param name="h">la altura del triángulo a calcular su área</param>
        /// <returns>El área del triángulo con 'B' y 'h' en los parámetros</returns>
        public decimal triangulo(decimal B, decimal h)
        {
            return B*h/2;
        }
        /// <summary>
        /// Calcula el área del cuadrado, mediante su LADO introducido en los parámetros.
        /// El resultado es de tipo decimal, así como el B y h de entrada.
        /// </summary>
        /// <param name="L">El lado del cuadrado a calcular su área</param>
        /// <returns>El área del cuadrado con lado L.</returns>
        public decimal cuadrado(decimal L)
        {
            return L * L;
        }
    }
}
